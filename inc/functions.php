<?php 
include_once('defines.php');
include_once('PHPMailer/PHPMailerAutoload.php');

function mysql_send($data) {
	global $DB;
	if ($data['step']==='first') {
		$sql = "INSERT INTO ".DB_TABLE." (name, phone, email, form, question, crm_project_id, referer, utm_source, utm_medium, utm_campaign, utm_term, utm_content, sent)
		VALUES (
			'".mysql_prepare($data['name'])."', 
			'".mysql_prepare($data['phone'])."', 
			'".mysql_prepare($data['email'])."', 
			'".mysql_prepare($data['form'])."', 
			'".mysql_prepare($data['question'])."',
			'".mysql_prepare($data['crm_project_id'])."', 
			'".mysql_prepare($_SERVER['HTTP_REFERER'])."', 
			'".mysql_prepare($_COOKIE['utm_source'])."', 
			'".mysql_prepare($_COOKIE['utm_medium'])."', 
			'".mysql_prepare($_COOKIE['utm_campaign'])."', 
			'".mysql_prepare($_COOKIE['utm_term'])."', 
			'".mysql_prepare($_COOKIE['utm_content'])."',
			'no'
		)";
	} else if ($data['step']==='second') {
		$sql = "UPDATE ".DB_TABLE."
		SET name='".mysql_prepare(substr($data['name'], 0, 255))."', 
			email='".mysql_prepare(substr($data['email'], 0, 255))."'
		WHERE id=".$data['id'];
	}
	
	if (mysqli_query($DB, $sql)) {
		$last_id = mysqli_insert_id($DB);
		if ($data['step']==='first') {
			return $last_id;
		}
	} else {
		return "Error: ".mysqli_error($DB);
	}
}

function mysql_sent_mark($id,$text) {
	global $DB;

	$sql = "UPDATE ".DB_TABLE."
	SET sent='".$text."'
	WHERE id='".$id."'";
	
	if (mysqli_query($DB, $sql)) {
		
	} else {
		return "Error: ".mysqli_error($DB);
	}
}

function mysql_sent_user_mark($id,$text) {
	global $DB;

	$sql = "UPDATE ".DB_TABLE."
	SET sent_user='".$text."'
	WHERE id='".$id."'";
	
	if (mysqli_query($DB, $sql)) {
		
	} else {
		return "Error: ".mysqli_error($DB);
	}
}

function mysql_prepare($text='') {
	$text = htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
	$text = stripslashes($text);
	$text = trim($text);
	return $text;
}

function crm_send($data) {
	global $DB;
	if (!empty($data['name'])) {$name=$data['name'];} else {$name='-';}
	if (!empty($data['email'])) {$email=$data['email'];} else {$email='mail@noreply.ru';}
	if (!empty($data['phone'])) {$phone=$data['phone'];} else {$phone='+00000000000';}
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_URL, 'http://form.itcmalik.com/');
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
	curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
		'name' => $name, // !Обязательно
		'email' => $email, // !Обязательно
		'phone' => $phone, // !Обязательно
		'project_id' => $data['crm_project_id'], // !Обязательно
		'city' => '',
		'referer' => $data['referer'],
		'utm_source0' => $data['form'],
		'utm_campaign0' =>  '',
		'utm_source' => $data['utm_source'], // !Обязательно если нужно записать остальные utm
		'utm_medium' => $data['utm_medium'],
		'utm_campaign' => $data['utm_campaign'],
		'utm_term' => $data['utm_term'],
		'utm_content' => $data['utm_content'],
	)));
	curl_setopt($curl, CURLOPT_HEADER, false);
	$res = curl_exec($curl);
	curl_close($curl);

	$sql = "UPDATE ".DB_TABLE."
		SET crmid = '".$res."'
		WHERE id=".$data['id'];
		
	mysqli_query($DB, $sql);
		
	return $res;
}
use \DrewM\MailChimp\MailChimp;
function mailchimp_send($data) {
	$phone = $email = '';
	if (!empty($data['email'])) {$email=$data['email'];}
	
	if (!empty($email)) {
		
		
		
		$MailChimp = new MailChimp(CHIMP_API);
		$list_id = CHIMP_ID;
		$result = $MailChimp->post("lists/$list_id/members", [
			'email_address' => $email,
			'status'        => 'subscribed',
		]);
		
		if ($MailChimp->success()) {
			$text =json_encode($result);	
		} else {
			$text =json_encode($MailChimp->getLastError());
		}
		
		global $DB;
		$sql = "UPDATE ".DB_TABLE."
			SET unisender='".$text."'
			WHERE id='".$data['id']."'";
		
		mysqli_query($DB, $sql);
		
		return $text;
	}
	
}


function send_to_user($email,$form, $id) {
	$sub = $mes = '';
	if ($form==='') {
		$sub = '';
		$mes = '';
	}

	if ($mes!='') {
		
		$address = $email; //заменить на свой email
		$from = 'From: '.SITE_NAME.' <no-reply@'.$_SERVER['SERVER_NAME'].'>';
		$headers = "Content-type:text/html; charset = UTF-8\r\n$from";
		
		$send = mail ($address,$sub,$mes,$headers);
		
		if(!$send) {
			mysql_sent_user_mark($id,'какая то проблема');
		} else {
			mysql_sent_user_mark($id,'yes');
		}
	}
}

function send_email($to,$sub,$mes) {
	$mail = new PHPMailer();
	$mail->CharSet = 'utf-8';
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = SMTP_SERVER;  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = SMTP_LOGIN;                 // SMTP username
	$mail->Password = SMTP_PASS;                           // SMTP password
	//$mail->SMTPSecure = 'tsl';                            // Enable TLS encryption, `ssl` also accepted
	$mail->SMTPSecure = false;
	$mail->SMTPAutoTLS = false;
	$mail->Port = SMTP_PORT;                                    // TCP port to connect to

	$mail->SMTPDebug = 4;

	$mail->setFrom(SMTP_LOGIN, SITE_URL);
	$mail->addAddress($to); 
	$mail->isHTML(true); 

	$mail->Subject = $sub;
	$mail->Body    = $mes;
	if(!$mail->send()) {
		echo 'Message to client could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	} else {
		echo 'Message to client has been sent';
	}
}

function send_partners_email($values) {
	$sub = SITE_URL." заявка из формы Стать патнером";
	$mes = "<h2>Получена заявка из формы Стать патнером.</h2>";
	$mes.= "<p>Имя: ".$values['name']."</p>";
	$mes.= "<p>Email: ".$values['email']."</p>";
	$mes.= "<p>Телефон: ".$values['phone']."</p>";
	send_email(PARTNERS_MANAGER_EMAIL,$sub,$mes);
}