<?php	

	$DB = mysqli_connect(DB_HOST, DB_USER, DB_PWD, DB_NAME);
	if (!$DB) {
		die("Connection failed: " . mysqli_connect_error());
	}

	$check = mysqli_query($DB,"select 1 from ".DB_TABLE." LIMIT 1");

	if($check !== FALSE)
	{
	   //DO SOMETHING! IT EXISTS!
	}
	else
	{
		// sql to create table
		$sql = "CREATE TABLE IF NOT EXISTS `".DB_TABLE."` (
		  `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
		  `phone` tinytext NOT NULL,
		  `name` tinytext NOT NULL,
		  `email` tinytext NOT NULL,
		  `form` text NOT NULL,
		  `question` longtext NOT NULL,
		  `crm_project_id` int(11) NULL,
		  `referer` text NOT NULL,
		  `utm_source` text NOT NULL,
		  `utm_medium` text NOT NULL,
		  `utm_campaign` text NOT NULL,
		  `utm_term` text NOT NULL,
		  `utm_content` text NOT NULL,
		  `sent` tinytext NOT NULL,
		  `sent_user` tinytext NOT NULL,
		  `crmid` text NOT NULL,
		  `unisender` text NOT NULL,
		  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;";



		if (mysqli_query($DB, $sql)) {
			
			echo "Table ".DB_TABLE." created successfully";
		} else {
			echo "Error creating table: " . mysqli_error($DB);
		}
		
		
		

	}