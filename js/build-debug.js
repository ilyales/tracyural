$(document).ready(function(){
  function resize_it() {
    if ($(window).width()>767) {
      $('body').removeClass('mobile-device');
      $("input[name='phone']").mask("+7 (999) 999-99-99");
    } else {
      $("input[name='phone']").unmask();
      $('body').addClass('mobile-device');
    }
  }

  $(window).on('resize', function() {
    resize_it();
  });

  resize_it();

  $(".header-menu-toggler").click(function() {
    $('.header-menu').toggleClass('opened');
  });


  $(document).on('click','.side-popup-toggle, .openside, .format-item, .side-popup-content .close', function() {
    $('body').toggleClass('side-opened');

  });

  initFacebookApi();
  $('.js-social-share-fb').click(function(){
    FB.ui({
      method: 'share',
      href: 'http://tracyural.ru',
    }, function(response){});
    return false;
  });

  $(".lazy").Lazy();
});

$(document).on('ready scroll', function() {
  var intro_h = $('#intro').height();
  var pos = $(window).scrollTop();
  //console.log(pos);
  if (pos>intro_h - 70) {
    if (!$('body').hasClass('showSideBtn')) {
      $('body').addClass('showSideBtn');
    }
  }
  else {
    if ($('body').hasClass('showSideBtn')) {
      $('body').removeClass('showSideBtn');
    }
  }
});
function transliterate(word){
  var a = {" ":"-","Ё":"YO","Й":"I","Ц":"TS","У":"U","К":"K","Е":"E","Н":"N","Г":"G","Ш":"SH","Щ":"SCH","З":"Z","Х":"H","Ъ":"-","ё":"yo","й":"i","ц":"ts","у":"u","к":"k","е":"e","н":"n","г":"g","ш":"sh","щ":"sch","з":"z","х":"h","ъ":"-","Ф":"F","Ы":"I","В":"V","А":"a","П":"P","Р":"R","О":"O","Л":"L","Д":"D","Ж":"ZH","Э":"E","ф":"f","ы":"i","в":"v","а":"a","п":"p","р":"r","о":"o","л":"l","д":"d","ж":"zh","э":"e","Я":"Ya","Ч":"CH","С":"S","М":"M","И":"I","Т":"T","Ь":"-","Б":"B","Ю":"YU","я":"ya","ч":"ch","с":"s","м":"m","и":"i","т":"t","ь":"-","б":"b","ю":"yu"};
  return word.split('').map(function (char) {
    return a[char] || char;
  }).join("");
}

$("input").keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
    $(this).closest('form').addClass('totrigger');
        $(".totrigger .btn-send").trigger('click');
        $(".totrigger").removeClass('totrigger');
    }
});

$(document).on('click','.btn-send', function(event) {
  event.preventDefault();
  $(this).closest('form').addClass('inwork');
  var data = $('.inwork').serialize(),
    errors = 0;
  $('.inwork .required').each( function() {
    $(this).removeClass('animated shake');
    if($(this).val()=='') {
      $(this).attr('placeholder',$(this).data('err')).addClass('has-error');

      errors=1;
    } else {
      $(this).removeClass('has-error');
    }
  });
  if (errors==0) {
    $.post('/inc/sender.php', data, aftersend);
  } else {
    $('.inwork').removeClass('inwork');
  }
});

$(document).on('click','.close', function() {
  $.magnificPopup.close()
});

$(document).on('click','.open-popup', function() {
  var href = $(this).attr('href');
  if (href=="#formats-poopup") {
    var formatType = $(this).attr('format-type'),
        formatTypeText,
        formName;
    if (formatType=="standart") {
      formatTypeText = "<b>«Стандарт»</b>";
      formName = "Узнать цену Стандарт";
    }
    else if (formatType=="business") {
      formatTypeText = "<b>«Бизнес»</b>";
      formName = "Узнать цену Бизнес";
    }
    else if (formatType=="vip") {
      formatTypeText = "<b>«ВИП»</b>";
      formName = "Узнать цену ВИП";
    }
    else if (formatType=="platinum") {
      formatTypeText = "<b>«Платинум»</b>";
      formName = "Узнать цену Платинум";
    }

    $('.js-formats-poopup-type').html(formatTypeText);
    $('.js-formats-poopup-form-name').attr("value",formName);
  }

  if (href=="#for-who-mobile-popup") {
    var type = $(this).attr('for-who-type'),
        formName;
    if (type=="predprinimateli") {
      formName = "Форма для кого Предприниматели и владельцы бизнеса";
    }
    else if (type=="top-managers") {
      formName = "Форма для кого Топ-менеджеры";
    }
    else if (type=="managers") {
      formName = "Форма для кого Менеджеры";
    }
    else if (type=="freelancers") {
      formName = "Форма для кого Фрилансеры";
    }
    else if (type=="bussines-trainers") {
      formName = "Форма для кого Бизнес-тренеры";
    }
    else if (type=="success-peoples") {
      formName = "Форма для кого Все кто хочет достигать максимума";
    }

    $('.js-for-who-mobile-popup-formname').attr("value",formName);
    // предотвратить закрытие блока
    $(this).closest('.forwho__item').addClass('visible');
  }

  if (href=="#networkform-poopup") {
    var type = $(this).attr('networking-type'),
        formName;
    if (type=="whatisit") {
      formName = "Форма нетворкинг что это";
    }
    else if (type=="vip") {
      formName = "Форма нетворкинг vip";
    }
    else if (type=="business") {
      formName = "Форма нетворкинг бизнес";
    }

    $('.js-networking-popup-formname').attr("value",formName);
  }

  $.magnificPopup.open({
    items: {
      src: href,

    },
    showCloseBtn: true,
    type: 'inline',
    callbacks: {
        open: function() {
          $('body').addClass('popup-opened');
        },
        close: function() {
          $('body').removeClass('popup-opened');
          if ($(window).width()>767) {

          }
        }
        }
  }, 0);
  return false;
});

function aftersend(data){
  var step = $('.inwork input[name="step"]').val();
  if (step==='first') {
    var phone = $('.inwork input[name="phone"]').val();
    var form = $('.inwork input[name="form"]').val();
    var email = $('.inwork input[name="email"]').val();
    var type = $('.inwork input[name="type"]').val();

    if (type === 'full') {
      $('body').removeClass('side-opened');


      var postfix = 'full';
      var postfix_ga = 'Полная форма';

      if (typeof yaCounter45562407 !== 'undefined') {
        yaCounter45562407.reachGoal(transliterate(form).toLowerCase()+'-'+postfix,function(){
          yaCounter45562407.reachGoal('redirect-to-events',function(){
            window.location.href = "https://malik-space-events.timepad.ru/event/561382/";
          });
        });
      }

    } else {
      $.magnificPopup.open({
        items: {
          src: '#additional'
        },
        closeOnBgClick: false,
        showCloseBtn: false,
        type: 'inline',
        callbacks: {
          open: function() {
            $('body').addClass('popup-opened');
            //$('.mfp-bg').remove();

          },
          close: function() {
            $('body').removeClass('popup-opened');
            if ($(window).width()>767) {

            }

          }
          // e.t.c.
          }
      }, 0);

      $('#additional input[name="email"]').val(email);
      $('#additional input[name="phone"]').val(phone);
      $('#additional input[name="form"]').val(form);
      $('#additional input[name="id"]').val(data);
      var postfix = 'first';
      var postfix_ga = 'Только телефон';
      if (typeof yaCounter45562407 !== 'undefined') {
        yaCounter45562407.reachGoal(transliterate(form).toLowerCase()+'-'+postfix);
        //yaCounter.reachGoal('sent-form');
        //ga('send', 'event', 'Отправка формы', form, postfix_ga);
        console.log(transliterate(form).toLowerCase()+'-'+postfix);
      }
    }

    $('.fancybox-close').remove();
  } else if (step==='second') {
    var form = $('.inwork input[name="form"]').val();
    $('.inwork .btn-send').addClass('sent');
    $.magnificPopup.open({
      items: {
        src: '#message-sent',

      },
      showCloseBtn: false,
      type: 'inline',
      callbacks: {
          open: function() {
            $('body').addClass('popup-opened');
            //$('.mfp-bg').remove();

          },
          close: function() {
            $('body').removeClass('popup-opened');
            if ($(window).width()>767) {

            }
          }
          // e.t.c.
          }
    }, 0);
    setTimeout(function(){
      $.magnificPopup.close()
    }, 3000);
    var postfix = 'second';
    var postfix_ga = 'Доп.форма';
    if (typeof yaCounter45562407 !== 'undefined') {
      console.log(transliterate(form).toLowerCase()+'-'+postfix);
      yaCounter45562407.reachGoal(transliterate(form).toLowerCase()+'-'+postfix);
      yaCounter45562407.reachGoal('sent-form');
      //ga('send', 'event', 'Отправка формы', form, postfix_ga);
      //ga('send', 'event', 'Отправка формы', 'Любая', 'С емейлом');
      console.log(transliterate(form).toLowerCase()+'-'+postfix);
    }
  }

  $('.fancybox-close').remove();
  $('.inwork input[type="text"], .inwork textarea').val('');
  $('.inwork input[name="question"]').val('');
  $('.inwork').removeClass('inwork');


}
$(document).ready(function(){

  $(".books-slider").owlCarousel({
    items: 4,
    dots: false,
    loop: true,
    nav: true,
    //navText:["<img src='/images/arrow-left.svg'>","<img src='/images/arrow-right.svg'>"],
    responsive : {
      // breakpoint from 0 up
      0 : {
        dots: true,
        nav: false,
        items: 1,
      },
      // breakpoint from 768 up
      768 : {
        dots: false,
        nav: true,
        items: 3,
      },
      // breakpoint from 768 up
      992 : {
        dots: false,
        nav: true,
        items: 4,
      }
    }
  });
  
  $(".quotes-items").owlCarousel({
    items: 1,
    dots: true,
    loop: true,
    nav: false,
    //navText:["<img src='/images/arrow-left.svg'>","<img src='/images/arrow-right.svg'>"],
    responsive : {
      // breakpoint from 0 up
      0 : {

        items: 1,
      },
      // breakpoint from 768 up
      768 : {
        items: 1,
      },
      // breakpoint from 768 up
      992 : {
        items: 2,
      }
    }
  });


  //слайдер фото-галереи
  $(".photos-slider__items").owlCarousel({
    items: 5,
    dots: false,
    loop: true,
    autoplay:true,
    autoplayTimeout:2000,

    nav: false,
    responsive : {
      0 : {
        items: 2,
        autoplay:true,
        autoplayTimeout:2000,
      },
      560 : {
        items: 3,
        autoplay:true,
        autoplayTimeout:2000,
      },
      768 : {
        items: 3,
        autoplay:true,
        autoplayTimeout:2000,
      },
      992 : {
        items: 4,
        autoplay:true,
        autoplayTimeout:2000,
      },
      1260 : {
        items: 5,
        autoplay:true,
        autoplayTimeout:2000,
      },
      1400 : {
        items: 5,
        autoplay:true,
        autoplayTimeout:2000,
      },
      1645: {
        items:6,
        autoplay:true,
        autoplayTimeout:2000,
      }
    }
  }); 

  $(document).on('click', "header .header-menu li:not(.delimiter), .scroll-to", function(){
    var target = $(this).data('scroll');
    $('html, body').animate({
      scrollTop: $(target).offset().top-55
    }, 500);
  });

  //слайдер для мобильной версии блока с Сергеем Озеровым
  (function(){
    $owl = $('.sj-oz__box-items');
    var carousel_Settings = {
      dots: false,
      items:1,
      nav:false,
      //animateOut: 'fadeOut',
      smartSpeed:100,
    };
    var owlStatus = "";
    function initialize(){
      if( $( window ).width() <= 767 && owlStatus!="on") {
        // initialize owl-carousel if window screensize is less the 767px
        $owl.addClass('owl-carousel owl-theme');
        $owl.owlCarousel( carousel_Settings );
        owlStatus = "on";
      } 
      else if( $( window ).width() > 767 && owlStatus!="off") {
        // destroy owl-carousel and remove all depending classes if window screensize is bigger then 767px
        $owl.trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
        $owl.find('.owl-stage-outer').children().unwrap();
        owlStatus = "off;";
      }
    }
    // initilize after window resize
    var id;
    $(window).resize( function() {
      clearTimeout(id);
      id = setTimeout(initialize, 500);
    });

    // initilize onload
    initialize();

    $('.sj-oz__box-items-arrows-prev').click(function(){
      $owl.trigger('prev.owl.carousel');
    });

    $('.sj-oz__box-items-arrows-next').click(function(){
      $owl.trigger('next.owl.carousel');
    });
  })();

  //раскрывающиеся блоки в "для кого мероприятие"
  (function(){

    $('.forwho__item').click(function(){
      if( $( window ).width() <= 767 ) {
        if (!$(this).hasClass('visible')) {
          $(this).addClass('visible');
        }
        else {  
          $(this).removeClass('visible');
        }
      }
      else {
        $(this).removeClass('visible');
      }      
    });
    
  })();

  //раскрывающиеся блоки в мобильной версии "форматы участия в тренинги"
  (function(){
    $('.js-formsInvolv-mob-item-head').click(function(){ 
    	var $item = $(this).parent('.js-ormsInvolv-mob-item');
      if (!$item.hasClass('visible')) {
        $item.addClass('visible');
      }
      else {
        $item.removeClass('visible');
      }
    });
  })();

  //слайдер "мероприятия которые мы орагнизовали"
  (function(){    
    var $currentNum = $('.js-measure-current-num'),
        $totalNum = $('.js-measure-total-num');
    
    $(".measures-ms__items").owlCarousel({
      items: 1,
      dots: false,
      loop: false,
      autoplay:false,
      autoplayTimeout:2000,
      nav: true,
      onChanged:changeCurrentNum,
      onInitialized:changeTotalNum,      
    }); 

    function changeCurrentNum(e) {
      $currentNum.text(e.item.index+1);
    }

    function changeTotalNum(e) {
      $totalNum.text("/ "+ e.item.count);
    } 

  })();


});
(function(){

ContentLazyLoad();

function ContentLazyLoad() {
  var VIDEO_INVITES_TRIGGER = 5300;
  var VIDEO_OTZIV_TRIGGER = 10100;
  var MAP_TRIGGER = 12500;
  var TRIGGER_BOTTOM = 3000;
  var GIS_SRC = '<iframe frameborder="no" style="border: 1px solid #a3a3a3; box-sizing: border-box;" width="100%" height="100%" src="http://widgets.2gis.com/widget?type=firmsonmap&amp;options=%7B%22pos%22%3A%7B%22lat%22%3A55.16074795453169%2C%22lon%22%3A61.41518354415894%2C%22zoom%22%3A17%7D%2C%22opt%22%3A%7B%22city%22%3A%22chelyabinsk%22%7D%2C%22org%22%3A%222111590608610909%22%7D"></iframe>';
  var MAP_WRAP_SELECTOR = ".js-map-wrap";

  init();

  function init() {
    $(window).on('scroll',loadVideoInvites);
    $(window).on('scroll',loadVideoOtziv);
    $(window).on('scroll',loadMap);
  }

  function loadVideoInvites() {
    var pos = $(window).scrollTop();
    if (pos>VIDEO_INVITES_TRIGGER && pos<VIDEO_INVITES_TRIGGER + TRIGGER_BOTTOM) {
      console.log('loadVideoInvites()');
      var videoInvitePlayers = plyr.setup('.video-invit__video');
      videoInvitePlayers[0].on('ready', function(event) {
        $('.js-video-invite-play-0').on("click",function(){
          console.log("play");
          event.detail.plyr.play();
          $(this).hide();
        });
      });
      videoInvitePlayers[1].on('ready', function(event) {
        $('.js-video-invite-play-1').on("click",function(){
          console.log("play");
          event.detail.plyr.play();
          $(this).hide();
        });
      });

      $(window).off('scroll',loadVideoInvites);
    }
  }

  function loadVideoOtziv() {
    var pos = $(window).scrollTop();
    if (pos>VIDEO_OTZIV_TRIGGER && pos<VIDEO_OTZIV_TRIGGER + TRIGGER_BOTTOM) {
      console.log('loadVideoOtziv()');
      var videoOtzivPlayer = plyr.setup('.video-otziv__video');
      videoOtzivPlayer[0].on('ready', function(event) {
        $('.js-video-otziv-play').on("click",function(){
          console.log("play");
          event.detail.plyr.play();
          $(this).hide();
        });
      });

      $(window).off('scroll',loadVideoOtziv);
    }
  }

  function loadMap() {
    var pos = $(window).scrollTop();
    if (pos>MAP_TRIGGER) {
      console.log('loadMap()');
      $mapWrap = $(MAP_WRAP_SELECTOR);
      $mapWrap.html(GIS_SRC);
      $(window).off('scroll',loadMap);
    }
  }
}

})();
