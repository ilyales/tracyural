(function(){

ContentLazyLoad();

function ContentLazyLoad() {
  var VIDEO_INVITES_TRIGGER = 5300;
  var VIDEO_OTZIV_TRIGGER = 10100;
  var MAP_TRIGGER = 12500;
  var TRIGGER_BOTTOM = 3000;
  var GIS_SRC = '<iframe frameborder="no" style="border: 1px solid #a3a3a3; box-sizing: border-box;" width="100%" height="100%" src="http://widgets.2gis.com/widget?type=firmsonmap&amp;options=%7B%22pos%22%3A%7B%22lat%22%3A55.16074795453169%2C%22lon%22%3A61.41518354415894%2C%22zoom%22%3A17%7D%2C%22opt%22%3A%7B%22city%22%3A%22chelyabinsk%22%7D%2C%22org%22%3A%222111590608610909%22%7D"></iframe>';
  var MAP_WRAP_SELECTOR = ".js-map-wrap";

  init();

  function init() {
    $(window).on('scroll',loadVideoInvites);
    $(window).on('scroll',loadVideoOtziv);
    $(window).on('scroll',loadMap);
  }

  function loadVideoInvites() {
    var pos = $(window).scrollTop();
    if (pos>VIDEO_INVITES_TRIGGER && pos<VIDEO_INVITES_TRIGGER + TRIGGER_BOTTOM) {
      console.log('loadVideoInvites()');
      var videoInvitePlayers = plyr.setup('.video-invit__video');
      videoInvitePlayers[0].on('ready', function(event) {
        $('.js-video-invite-play-0').on("click",function(){
          console.log("play");
          event.detail.plyr.play();
          $(this).hide();
        });
      });
      videoInvitePlayers[1].on('ready', function(event) {
        $('.js-video-invite-play-1').on("click",function(){
          console.log("play");
          event.detail.plyr.play();
          $(this).hide();
        });
      });

      $(window).off('scroll',loadVideoInvites);
    }
  }

  function loadVideoOtziv() {
    var pos = $(window).scrollTop();
    if (pos>VIDEO_OTZIV_TRIGGER && pos<VIDEO_OTZIV_TRIGGER + TRIGGER_BOTTOM) {
      console.log('loadVideoOtziv()');
      var videoOtzivPlayer = plyr.setup('.video-otziv__video');
      videoOtzivPlayer[0].on('ready', function(event) {
        $('.js-video-otziv-play').on("click",function(){
          console.log("play");
          event.detail.plyr.play();
          $(this).hide();
        });
      });

      $(window).off('scroll',loadVideoOtziv);
    }
  }

  function loadMap() {
    var pos = $(window).scrollTop();
    if (pos>MAP_TRIGGER) {
      console.log('loadMap()');
      $mapWrap = $(MAP_WRAP_SELECTOR);
      $mapWrap.html(GIS_SRC);
      $(window).off('scroll',loadMap);
    }
  }
}

})();
