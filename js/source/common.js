$(document).ready(function(){
  function resize_it() {
    if ($(window).width()>767) {
      $('body').removeClass('mobile-device');
      $("input[name='phone']").mask("+7 (999) 999-99-99");
    } else {
      $("input[name='phone']").unmask();
      $('body').addClass('mobile-device');
    }
  }

  $(window).on('resize', function() {
    resize_it();
  });

  resize_it();

  $(".header-menu-toggler").click(function() {
    $('.header-menu').toggleClass('opened');
  });


  $(document).on('click','.side-popup-toggle, .openside, .format-item, .side-popup-content .close', function() {
    $('body').toggleClass('side-opened');

  });

  initFacebookApi();
  $('.js-social-share-fb').click(function(){
    FB.ui({
      method: 'share',
      href: 'http://tracyural.ru',
    }, function(response){});
    return false;
  });

  $(".lazy").Lazy();
});

$(document).on('ready scroll', function() {
  var intro_h = $('#intro').height();
  var pos = $(window).scrollTop();
  //console.log(pos);
  if (pos>intro_h - 70) {
    if (!$('body').hasClass('showSideBtn')) {
      $('body').addClass('showSideBtn');
    }
  }
  else {
    if ($('body').hasClass('showSideBtn')) {
      $('body').removeClass('showSideBtn');
    }
  }
});