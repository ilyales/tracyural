'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var streamqueue  = require('streamqueue');
var autoprefixer = require('gulp-autoprefixer');
var uglifycss = require('gulp-uglifycss');
var minify = require('gulp-minify');
var merge = require('merge-stream');
var watch = require('gulp-watch');


gulp.task('build-styles', function() {

  var scssStream = gulp.src('./css/source/scss/**/*.scss')
      .pipe(sass({outputStyle:"expanded"}).on('error', sass.logError))
      .pipe(concat('scss-files.scss'))
  ;

  var cssStream = gulp.src('./css/source/**/*.css')
      .pipe(concat('css-files.css'))
  ;

  var mergedStream = merge(cssStream,scssStream)
      .pipe(concat('build2.css'))
      .pipe(autoprefixer({
          browsers: ['last 3 versions','ie 9'],
          cascade: false
      }))
      .pipe(uglifycss({
        "maxLineLen": 80,
        "uglyComments": true
      }))
      .pipe(gulp.dest('./css'));

  return mergedStream;
});

gulp.task('build-js', function() {
  return streamqueue({ objectMode: true },
      gulp.src('./js/source/common.js'),
      gulp.src('./js/source/popups.js'),
      gulp.src('./js/source/sliders.js'),
      gulp.src('./js/source/lazyload.js')
    )
    .pipe(concat('build.js'))
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        }
    }))
    .pipe(gulp.dest('./js'));
});


gulp.task('default',function(){
  gulp.start('build');

  watch(['./css/source/scss/**/*.scss','./css/source/**/*.css'], function() {
      gulp.start('build-styles');
  });
  watch(['./js/source/**/*'], function() {
      gulp.start('build-js');
  });
});


gulp.task('build', ['build-styles','build-js']);