<?php
include_once('../inc/defines.php');
include_once('../inc/access.php');
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<title>Заявки <?php echo SITE_NAME; ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<meta name="MobileOptimized" content="device-width"/>
	<meta name="format-detection" content="telephone=no">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap/css/bootstrap-theme.min.css" type="text/css" />
	<link rel="shortcut icon" href="../favicon.ico">
</head>
<body id="home">
	<div class="container">